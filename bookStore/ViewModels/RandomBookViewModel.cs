﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using bookStore.Models;

namespace bookStore.ViewModels
{
    public class RandomBookViewModel
    {
        public Book Book { get; set; }
        public List<Customer> Customers { get; set; }
}
}